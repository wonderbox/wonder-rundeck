#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")

create_and_clean_dist_dir(){
  if [ ! -d ${script_path}/dist ]; then
    mkdir -p ${script_path}/dist
  else 
    rm -rf ${script_path}/dist/*
  fi
}

write_version_with_revision(){
  echo "$(cat ${script_path}/version.txt)-$(cd ${script_path}&& git rev-parse HEAD)" > ${1}/${2}-version.txt
}


create_script_jobs_package(){
  echo "=> Start creating rundeck nodes package"
  tmp_dir=$(mktemp -d -t jobs-XXXXXXXXXX)
  cp -r ${script_path}/jobs/scripts ${tmp_dir}
  write_version_with_revision ${tmp_dir} "scripts"
  pushd .
  cd  ${tmp_dir} &&  tar -cvf ${script_path}/dist/jobs-scripts-$(cat ${script_path}/version.txt).tar.gz *
  popd 
 
}

create_rundeck_nodes_package(){
  echo "=> Start creating rundeck nodes package"
  tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
  cp -r ${script_path}/rundeck/conf ${tmp_dir}
  write_version_with_revision ${tmp_dir} "rundeck"
  pushd .
  cd  ${tmp_dir} &&  tar -cvf ${script_path}/dist/rundeck-conf-$(cat ${script_path}/version.txt).tar.gz *
  popd 
  
}

create_jobs_definition_package(){
  echo "=> Start creating rundeck jobs definition package"
  tmp_dir=$(mktemp -d -t jobs-definition-XXXXXXXXXX)
  cp -r ${script_path}/rundeck/jobs-definition ${tmp_dir}
  write_version_with_revision ${tmp_dir} "jobs-definition"
  pushd .
  cd  ${tmp_dir} &&  tar -cvf ${script_path}/dist/rundeck-jobs-definition-$(cat ${script_path}/version.txt).tar.gz *
}

create_and_clean_dist_dir
create_script_jobs_package
create_rundeck_nodes_package
create_jobs_definition_package
