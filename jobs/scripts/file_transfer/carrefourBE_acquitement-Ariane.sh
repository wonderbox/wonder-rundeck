#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/carrefourBE/acquittements

function setParamsCarrefourBEAcq {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /carrefour_be/out '*.txt' delete
		setDestFTP SFTP sftp.carrefourbelgium.be wonderbox '#w0nD3rB0Xx$2F7p' /prd/ToCB/ '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/carrefourBE/out exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP FTP WPPDFTP1 wonderbox Yuzi6Ahw /carrefour_be/out '*.txt' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/carrefour_be/out '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/carrefourBE/out tangi.vass@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsCarrefourBEAcq $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
