#!/bin/bash
# set -x
# todo check key authentication
#todo rewrite this script
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

echo `date`


cd "${script_path}"

if [ "$1" = "" ]
then
	echo "Usage:"
	echo "  FTPCarrefourFr.sh [PROD|DEV]"
	exit 0
fi


export TMP_DIR=${jobs_scripts_tmp_path}/carrefour
export TARGETEMAILSTD=exploitation@wonderbox.fr

if [ -d $TMP_DIR ]
then
        ps -ef | grep FTPCarefourIT.sh | grep -v grep > /dev/null 2>&1
        if [ $? -eq 0 ]
        then
                export ERROR="Le processus ${script_path}/FTPCarrefour.sh est toujours en train d'executer sur le serveur $HOSTNAME"
        else
                rmdir $TMP_DIR
                if [ $? -ne 0 ]
                then
                        export ERROR="Le repertoire $TMP_DIR n'a pas pu supprime sur le serveur $HOSTNAME"
                fi
        fi
fi

if [ -n "$ERROR" ]
then
        cat <<EOF | mutt -s "Erreur avec le processus FTPCarrefour de $1 sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Il semble que le processus FTPCarrefour de $1 peut avoir un problem sur le serveur $HOSTNAME. Verifier qu'il execute encore et sinon, supprimer le repertoire $TMP_DIR.

Error:
'$ERROR'

Cordialement,
EOF

        exit 1
fi


if [ ! -d $TMP_DIR ]
then
	mkdir -p $TMP_DIR
fi

if [ "PROD" = "$1" ]
then
	export IP_FTP_CARREFOUR=62.23.8.35
	export USERID_REMOTE=wonderbox
	# export PASSWORD_REMOTE="wOnderBo\\/"
	export PASSWORD_REMOTE="iU5d$nQ2!bV"
	export IP_FTP_WB=wprdftp1.wonderbox.lan
	export USERID_WB=carrefour
	export PASSWORD_WB=Siuw1pnE
else
	export IP_FTP_CARREFOUR_IT=192.168.111.11
	export USERID_REMOTE=wonderbox
	export PASSWORD_REMOTE=wOnderBo\\/
	export IP_FTP_WB=192.168.111.11
	export USERID_WB=carrefour
	export PASSWORD_WB=Siuw1pnE
fi

# Recuperer itous les fichiers depuis le serveur FTP d'Carrefour

echo Recuperer les fichiers du serveur remote de Carrefour `date`
cd $TMP_DIR

cat <<EOF | sftp -o "IdentityFile /home/wbx/.ssh/talend_maya_id_rsa" -b - ${USERID_REMOTE}@${IP_FTP_CARREFOUR} 
cd Export/Sales
mget *.csv
quit
EOF

# Tous les fichiers dans $TMP_DIR sont bien recuperes donc...

for f in `ls`
do
	echo Transferer le fichier $f vers le FTP wonderbox

	# Renommer le fichier de csv vers txt provisoirement jusqu'a janvier quand
	# le changement du fichier XML soit installe.

#	if [ "`echo $f | grep .csv`" != "" ]
#	then
#		newFile=`echo $f | sed -n 's/csv/txt/p'`
#		echo Renaming $f to $newFile
#		mv $f $newFile
#	else
#		newFile=$f
#	fi

	cat <<EOF | ftp -i -n >/dev/null 2>&1
open $IP_FTP_WB
user $USERID_WB $PASSWORD_WB
cd in
put $f
quit
EOF
	
	# ... puis deplacer le vers le repertoire BKUP sur le serveur de Carrefouri
        # (en utilisant un mput puis un mdelete

	echo Deplacer $f vers BKP

	cat <<EOF | sftp -o "IdentityFile /home/wbx/.ssh/talend_maya_id_rsa" -b - ${USERID_REMOTE}@${IP_FTP_CARREFOUR} 
cd Export/Sales
rename $f BKUP/$f
quit
EOF

done

rm -f $TMP_DIR/*
rmdir $TMP_DIR


