#!/bin/bash
source ${script_path}/../common/Ariane-Transfer-Functions.sh

BASE_DIR="${backup_base_dir}"
PARTNER="tws"
NOTIFY="exploitation@wonderbox.fr"
DAY="1 days ago"
#echo `date --date="$DAY" '+date : %Y/%m/%d'`
DAILY_FILE_NUMBER=`ls -lart ${BASE_DIR}/${PARTNER}/*/$(date --date="$DAY" '+%Y/%m/%d')/*.psv | wc -l` 
if [ ${DAILY_FILE_NUMBER} -gt 0 ]
then
     REPORT="Nombre de fichiers echanges avec ${PARTNER} le $(date --date="$DAY" '+%d/%m/%Y') : $DAILY_FILE_NUMBER"
     FILE_LIST=`ls -lart ${BASE_DIR}/${PARTNER}/*/$(date --date="$DAY" '+/%Y/%m/%d')/*.psv`
     REPORT="$REPORT"$'\n\n'
     REPORT="${REPORT}Liste des fichiers :"$'\n'"$FILE_LIST";
else
     #echo "AUCUN fichier echange avec ${PARTNER} le $(date --date="$DAY" '+%d %m %Y') !!!!!!\n";
     REPORT="AUCUN fichier echange avec ${PARTNER} le $(date --date="$DAY" '+%d %m %Y') !!!!!!"$'\n';
	
fi 
#echo "${REPORT}"
echo "-> envoi du rapport par mail a $NOTIFY ..."
echo "${REPORT}" | mailx -s "Rapport d'echange fichier avec $PARTNER pour la journee du $(date --date="$DAY" '+%d/%m/%Y')" $NOTIFY
echo "done." 
