#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/carrefourES


function setParamsCarrefourES {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP f50ftp.carrefour.es wonder w4nd2r '/home/wonder' '*.tar.gz' delete
		setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /carrefour_es '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/carrefour_es/ACT exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	else
		setSrcFTP SFTP 192.168.111.11 wonderbox '' /home/ftp/wonderbox/carrefour_es/ '*.tar.gz' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/carrefour_es '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/carrefour_es/ACT tangi.vass@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	fi
}



main $1
setParamsCarrefourES $1
checkDryRunMode
checkReadiness
getFiles
unzipAndBackupFiles
putFiles
cleanup

exit 0
