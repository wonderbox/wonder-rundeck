#!/bin/bash
# set -x
# todo check this script again
# todo rewrite this script

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
dry_run_mode(){
   if [ "${DRY_RUN}" == "true" ]; then
    echo "Test Connexion mode"
    echo "Connexion vers wonderbox"
   	cat <<EOF | ftp -i -n >/dev/null 2>&1
    open $IP_FTP_WB
    user $USERID_WB $PASSWORD_WB
    cd $WBX_FTP_BASE_DIR/ACT
    ls 
    quit
EOF
    
    echo "Connexion vers auchan it"
    cat <<EOF | ftp -i -n >/dev/null 2>&1
open $IP_FTP_AUCHAN_IT
user $USERID_REMOTE $PASSWORD_REMOTE
cd $REMOTE_BASE_DIR/upload/ACT
lcd $TMP_DIR
get $filename
quit
EOF

   exit 0
   fi
}

cd "${script_path}"

#echo `whoami` > a.out

if [ "$1" = "" ]
then
	echo "Usage:"
	echo "  FTPAuchanIt.sh [PROD|DEV]"
	exit 0
fi

if [ "PROD" = "$1" ]
then
	export IP_FTP_AUCHAN_IT=fileport01.auchan.it
	export USERID_REMOTE=wonderbox
	export PASSWORD_REMOTE=1409Alighieri1321
	export IP_FTP_WB=ftp.wonderbox.vpn
	export USERID_WB=auchan_it
	export PASSWORD_WB=LJXVbXT6
	export REMOTE_BASE_DIR=.
	export WBX_FTP_BASE_DIR=.
else
	export IP_FTP_AUCHAN_IT=192.168.111.11
	export USERID_REMOTE=wonderbox
	export PASSWORD_REMOTE=Yuzi6Ahw
	export IP_FTP_WB=192.168.111.11
	export USERID_WB=wonderbox
	export PASSWORD_WB=Yuzi6Ahw
	export REMOTE_BASE_DIR=/dev/auchan_it_remote
	export WBX_FTP_BASE_DIR=/dev/auchan_it
fi

export TMP_OK_DIR=${jobs_scripts_tmp_path}/auchanIt/okfiles
export TMP_DIR=${jobs_scripts_tmp_path}/auchanIt
export TARGETEMAILSTD=exploitation@wonderbox.fr

if [ -d $TMP_DIR ]
then
        ps -ef | grep FTPAuchaniIT.sh | grep -v grep > /dev/null 2>&1
        if [ $? -eq 0 ]
        then 
		export ERROR="Le processus ${script}/FTPAuchanIT.sh est toujours en train d'executer sur le serveur $HOSTNAME"
	else
		rmdir $TMP_DIR
		if [ $? -ne 0 ]
		then
			export ERROR="Le repertoire $TMP_DIR n'a pas pu supprime sur le serveur $HOSTNAME"
		fi
	fi
fi

if [ -n "$ERROR" ]
then
	cat <<EOF | mutt -s "Erreur avec le processus FTPAchanIT de $1 sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Il semble que le processus FTPAuchanIT de $1 peut avoir un problem sur le serveur $HOSTNAME. Verifier qu'il execute encore et sinon, supprimer le repertoire $TMP_DIR.

Error:
'$ERROR'

Cordialement,
EOF

	exit 1
fi
# activate dry run mode if needed
dry_run_mode 

if [ ! -d $TMP_OK_DIR ]
then
	mkdir -p $TMP_OK_DIR
fi

if [ ! -d $TMP_DIR ]
then
	mkdir -p $TMP_DIR
fi



# Recuperer tous les fichiers depuis le serveur FTP d'Auchan

echo Recuperer les fichiers du serveur remote d Auchan `date`
cd $TMP_OK_DIR

cat <<EOF | ftp -i -n
open $IP_FTP_AUCHAN_IT
user $USERID_REMOTE $PASSWORD_REMOTE
cd $REMOTE_BASE_DIR/upload/ACT
mget *.OK
quit
EOF

# Pour chaque fichier .OK recupere, enlever le .OK du nom et faire un FTP get

for f in `ls *.OK 2>/dev/null`
do
	filename=`echo $f | sed -n 's/\(.*\)\.OK/\1/p'`

	cat <<EOF | ftp -i -n >/dev/null 2>&1
open $IP_FTP_AUCHAN_IT
user $USERID_REMOTE $PASSWORD_REMOTE
cd $REMOTE_BASE_DIR/upload/ACT
lcd $TMP_DIR
get $filename
quit
EOF
	if [ -f $TMP_DIR/$filename ]
	then
		mv $f $TMP_DIR
	fi
done

# Tous les fichiers dans $TMP_DIR sont bien recuperes donc...

cd $TMP_DIR

for f in `ls`
do
	filename=`echo $f | sed -n 's/\(.*\)\.OK/\1/p'`

        # ... si le nom ne termine pas avec .OK transferer le vers le serveur Wonderbox...
	if [ "$filename" = "" ]
	then
		echo Transferer le fichier $f vers le FTP wonderbox $WBX_FTP_BASE_DIR/ACT
		cat <<EOF | ftp -i -n >/dev/null 2>&1
open $IP_FTP_WB
user $USERID_WB $PASSWORD_WB
cd $WBX_FTP_BASE_DIR/ACT
put $f 
quit
EOF
	
	fi

	# ... puis deplacer les vers le repertoire BKUP sur le serveur d Auchan (en utilisant
	# un mput puis un mdelete

	echo Deplacer $f vers BKP

	cat <<EOF | ftp -i -n >/dev/null 2>&1
open $IP_FTP_AUCHAN_IT
user $USERID_REMOTE $PASSWORD_REMOTE
cd $REMOTE_BASE_DIR/upload/ACT
rename $f BKP/$f
quit
EOF

done

# Supprimer les fichiers dans $TMP_OK_DIR et $TMP_DIR

rm -f $TMP_OK_DIR/*
rmdir $TMP_OK_DIR
rm -f $TMP_DIR/*
rmdir $TMP_DIR


