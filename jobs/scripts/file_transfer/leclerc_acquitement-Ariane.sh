#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/leclercFR/acquittements


function setParamsLeclercAcq {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /distributeurs/leclerc/out '*.zip' delete
		#NB : Leclerc (Infomil) a un serveur backup sur 172.27.8.65, mais nous ne savons pas basculer automatiquement
		# 172.27.8.34 Serveur principale
		# 172.27.8.66 Serveur de secours
		setDestFTP FTP 172.27.8.34 uftpedt0006 pwdedt0006 . '*.zip'
		setLocalParams ${jobs_scripts_tmp_path}/leclerc/out exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP FTP WPPDFTP1 wonderbox Yuzi6Ahw /distributeurs/leclerc/out '*.zip' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/leclerc/out '*.zip'
		setLocalParams ${jobs_scripts_tmp_path}/leclerc/out tangi.vass@wonderbox.fr  ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsLeclercAcq $1
checkDryRunMode
checkReadiness
getFiles
#for i in *.ZIP ; do mv $i `basename $i .ZIP`.zip ; done
putFiles
moveFilesToBackup
cleanup

exit 0
