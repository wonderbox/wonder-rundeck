#!/bin/bash
# Script de transfert de fichiers entre SAP et le partage export_fec
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh


BKUP_DIR_TAR=${backup_base_dir}/sap/extract_fec

TARGET_SAP_SERVER="wbx-prd-as01.wonderbox.vpn"
# not good but we must specify the full path of the target dir
SFTP_IDENTITY="/var/lib/rundeck/.ssh/old-keys/sched01/rundeck_id_rsa"
#Mode for migration
if [ "${DRY_RUN}" == "true" ]; then
  echo "We are in the dry run mode"
fi

function setParamsSAP {
        setSrcFTP SFTP  wbx-prd-as01.wonderbox.vpn ta_oasis "" /oasis/FEC/in '*' delete 
        setDestFTP LOCAL wbx-prd-sched01.wonderbox.vpn rundeck "" /opt/cifs/export_fec '*'
        setLocalParams /opt/cifs/export_fec/tmp/ exploitation@wonderbox.fr /opt/cifs/export_fec/backup/`date +%Y/%m/%d`
}

main PROD
setParamsSAP
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

echo "nettoyage des backup"
rm -rf /opt/cifs/export_fec/backup/*

exit 0
