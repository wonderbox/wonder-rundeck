#!/bin/bash
#todo fixme
SRC_DIR=/opt/cifs/exalog/transfer_orders
DST_DIR=/workflow/AMB_SAP
FILES="*"
DARCH="arch/$(date '+%Y')/$(date '+%m')/$(date '+%d')"

SFTP_IDENTITY="/var/lib/rundeck/.ssh/old-keys/sched01/rundeck_id_rsa"

count=0
res=0
cd $SRC_DIR
if [ $? != 0 ] ; then
       echo "Probleme acces repertoire $SRC_DIR. Exiting..."
       exit 1
fi

for file in `ls -1`
do
	if [ -f $file ]
	then 

		cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - -P 7022 wonderbox@ftpbank7.exalog.net
cd $DST_DIR
put $file
EOF
		if [ $? == 0 ] ; then
			if [ ! -d $DARCH ] ; then
				mkdir -p $DARCH
			fi
			mv $file $DARCH
		else
			echo "Probleme de transfert avec $file. Fichier non transfere."
			echo "Probleme de transfert avec $file." | mailx -s "[Alerte]Exabank PushOrders - fichier non transfere" it@wonderbox.fr
			res=1
		fi
		count=`expr $count + 1`
	fi
done
if [ $res == 0 ] ; then
	if [ $count == 0 ] ; then
		echo "Pas de fichier à transférer."
	else
		echo "$count fichiers transférés."
	fi
fi
exit $res
