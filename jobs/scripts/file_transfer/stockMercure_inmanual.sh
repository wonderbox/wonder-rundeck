#!/bin/bash

# Script de transfert de fichiers stock mercure in_manual
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

BKUP_DIR_TAR=${backup_base_dir}/mercure/stock_in_manual




function setParamsEXA {
        setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/in '*.psv' delete
        setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/in_manual '*.psv'
        setLocalParams ${jobs_scripts_tmp_path}/mercure/stock_in_manual exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
}

main PROD
setParamsEXA
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
