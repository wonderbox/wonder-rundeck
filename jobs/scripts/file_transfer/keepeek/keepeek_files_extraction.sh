#!/bin/bash
echo 'extraction des archives keepeek'
cd /opt/wonderbox/ftp/keepeek/export

for i in /opt/wonderbox/ftp/keepeek/export/*.zip;
do
          export baseNameFile=`basename $i .zip`;
          unzip "$i" -d "$baseNameFile";
done

rm -f /opt/wonderbox/ftp/keepeek/export/*.zip

