#!/bin/bash
export SOURCE_DIRECTORY=/opt/wonderbox/ftp/keepeek/export
echo "_____ "
echo "_____ "
echo "_____ "
echo "Démarrage du transfert des fichiers vers ftp-preprod "
for file in $SOURCE_DIRECTORY/*
do
   if [[ -f ${file} ]]; then
      echo "Transferring file ${file}"
      lftp -u 'keepeek_sftp,PkpGihYsEPFY3o6u' -e "pwd ; cd export ; mput ${file} ; quit" sftp://wbx-ftppp-01.wonderbox.vpn
      rm ${file}
   fi
done

echo "_____ "
echo "_____ "
echo "_____ "
echo "Fin du transfert des fichiers vers ftp-preprod, clean du répertoire "
echo "Clean du répertoire terminé."
