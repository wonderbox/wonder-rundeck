#!/bin/bash
# Script de transfert de fichiers entre SAP et le partage export_factu
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh


BKUP_DIR_TAR=${backup_base_dir}/sap/extract_pnl

#todo check this mouting point
rm -f /opt/cifs/extract_pnl/previous/*.xlsx
mv /opt/cifs/extract_pnl/*.xlsx /opt/cifs/extract_pnl/previous/

TARGET_SAP_SERVER="wbx-prd-as01.wonderbox.vpn"
# not good but we must specify the full path of the target dir
SFTP_IDENTITY="/var/lib/rundeck/.ssh/old-keys/sched01/rundeck_id_rsa"
#Mode for migration
if [ "${DRY_RUN}" == "true" ]; then
  echo "We are in the dry run mode"
fi

function setParamsSAP {
        setSrcFTP SFTP  wbx-prd-as01.wonderbox.vpn ta_oasis "" /oasis/Extract_PNL '*' delete 
        setDestFTP LOCAL wbx-prd-sched01.wonderbox.vpn rundeck "" /opt/cifs/extract_pnl '*'
        setLocalParams ${jobs_scripts_tmp_path}/sap/extract_pnl/ exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
}

main PROD
setParamsSAP
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
