#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/intersolve

function setParamsIntersolveVIVA {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP SFTP sftp.intersolve.nl wonderbox '' Export/Vivabox '*.psv' delete
		setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} intersolve/in '*.psv' # meme endroit que les act be pour viva
		setLocalParams /opt/wonderbox/mule/fr/var/run/tmp/Intersolve_viva/ACT exploitation@wonderbox.fr /opt/wonderbox/mule/fr/var/backups/IntersolveVIVA
	fi
}



main $1
setParamsIntersolveVIVA $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
