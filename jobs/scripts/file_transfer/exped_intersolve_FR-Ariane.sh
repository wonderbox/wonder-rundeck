#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/intersolve


function setParamsExpedIntersolve_FR {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /intersolve/out/livFR '*.csv' delete
		setDestFTP SFTP sftp.intersolve.nl wonderbox '' Import '*.csv'
		setLocalParams ${jobs_scripts_tmp_path}/intersolve/EXP_FR exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsExpedIntersolve_FR $1
checkDryRunMode
checkReadiness
getFiles
putFiles
notifyExped 'INTERSOLVE_FR EXPED'
moveFilesToBackup
cleanup

exit 0
