#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/alcampo/acquittements

function setParamsAlcampoAcq {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP ftp.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /alcampo_es/ack '*.txt' delete
		setDestFTP SFTP ftp.wonderbox.fr alcampo_es s2h3L7Bk /upload/ack '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/alcampo/out exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/alcampo_es/output '*.txt' keep
		setDestFTP SFTP 192.168.111.11 wonderbox '' /home/ftp/wonderbox/alcampo_es/output '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/alcampo/out tangi.vass@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsAlcampoAcq $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
