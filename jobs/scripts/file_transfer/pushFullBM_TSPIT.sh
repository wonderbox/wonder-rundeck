#!/usr/bin/env bash

set -eo pipefail

echo '██████████▒░ CAPTURE DATABASE ENVIRONMENT ░▒██████████'

STOCK_BM_FILENAME=$1
EXPED_DIR=/opt/wonderbox/ftp/tsp_it/output
WORK_DIR=$EXPED_DIR/manual
PATH_TO_STOCK_BM_FILE=$WORK_DIR/$STOCK_BM_FILENAME
WORK_BM_FILENAME=STOCK_EXPED_`date +%Y`-sorted-n-uniq
PATH_TO_WORK_BM_FILE=$WORK_DIR/$WORK_BM_FILENAME
PATH_TO_WORK_BM_BACKLOG_FILE=$PATH_TO_WORK_BM_FILE.backlog

NB_LINES_LIMIT=45000
DRY_RUN=0

function logit()
{
    echo "[${USER}][`date`] - ${*}"
}

function usage()
{
cat << EOF
usage: $0 [stock bm file]

OPTIONS:
   stock bm file :             un fichier de stock BM
   -d|--dry-run :              mode dry-run
   -h                          Show this message
EOF
exit
}

while [ $# -gt 0 ]
do
    case "$1" in
       -d|--dry-run)
           echo "Mode dry run enabled"
           DRY_RUN=1;shift;;
       -h|--help)
           usage;;
       -*)
           usage;;
       --)
           break;;
       *)
           shift;;
    esac
done

if [ ! -f $PATH_TO_STOCK_BM_FILE ]
then
    logit 'No BM Stock file found'
    exit 1
fi

if [[ -z $STOCK_BM_FILENAME ]] || [[ "$STOCK_BM_FILENAME" =~ ^-.* ]]
then
    read -r -p 'Stock BM filename : ' STOCK_BM_FILENAME
else
    logit "Stock BM filename : $STOCK_BM_FILENAME"
fi

PATH_TO_WORK_BM_BACKLOG_OF_THE_DAY_FILE=$PATH_TO_WORK_BM_BACKLOG_FILE.`date +%Y%m%d`

# Suppress lines from backlog
if [ -f $PATH_TO_WORK_BM_BACKLOG_OF_THE_DAY_FILE ]
then
  logit "Backlog of DCS to send has already been worked today (`date +%Y%m%d` - $PATH_TO_WORK_BM_BACKLOG_OF_THE_DAY_FILE )"
  exit 2
fi

# Sort and delete doublons
if [ ! -f $PATH_TO_WORK_BM_FILE ]
then
  sort -nr $PATH_TO_STOCK_BM_FILE | uniq > $PATH_TO_WORK_BM_FILE
  #Init backlog file of DCS left to treat
  cat $PATH_TO_WORK_BM_FILE > $PATH_TO_WORK_BM_BACKLOG_FILE
else
  logit "$PATH_TO_WORK_BM_FILE already exists./n -> If you want to generate a new one : rm -f $PATH_TO_WORK_BM_FILE before executing this script"
fi

#find last EXPED files

EXPED_DIR_FOR_SED=$(echo $EXPED_DIR | sed 's/\//\\\//g')
LAST_EXPED_FILE=$(find $EXPED_DIR -maxdepth 1 -type f -name "EXPED*" -mtime -1 | head -1 | sed 's/'${EXPED_DIR_FOR_SED}'\///g')
logit "Last EXPED* file found : $LAST_EXPED_FILE"

cp $EXPED_DIR/$LAST_EXPED_FILE $WORK_DIR/$LAST_EXPED_FILE.orig
cat $WORK_DIR/$LAST_EXPED_FILE.orig > $WORK_DIR/$LAST_EXPED_FILE.reworked
cat $PATH_TO_WORK_BM_BACKLOG_FILE >> $WORK_DIR/$LAST_EXPED_FILE.reworked
cat $WORK_DIR/$LAST_EXPED_FILE.reworked | head -n $NB_LINES_LIMIT > $WORK_DIR/$LAST_EXPED_FILE || echo "Cut $LAST_EXPED_FILE after $NB_LINES_LIMIT lines"
rm -f $WORK_DIR/$LAST_EXPED_FILE.reworked
logit "-- Part of BM exported to new $LAST_EXPED_FILE file"

if [ $DRY_RUN -eq 0 ]
then
  logit "-- Suppress DCS sent from backlog of DCS to send"
  cp $PATH_TO_WORK_BM_BACKLOG_FILE $PATH_TO_WORK_BM_BACKLOG_OF_THE_DAY_FILE
  #join -v1 -v2  $WORK_DIR/$LAST_EXPED_FILE $PATH_TO_WORK_BM_BACKLOG_FILE > $PATH_TO_WORK_BM_BACKLOG_FILE.tmp
  grep -vxf $WORK_DIR/$LAST_EXPED_FILE $PATH_TO_WORK_BM_BACKLOG_FILE > $PATH_TO_WORK_BM_BACKLOG_FILE.tmp
  mv $PATH_TO_WORK_BM_BACKLOG_FILE.tmp $PATH_TO_WORK_BM_BACKLOG_FILE

  logit "-- Move $WORK_DIR/$LAST_EXPED_FILE to $EXPED_DIR/$LAST_EXPED_FILE"
  mv $WORK_DIR/$LAST_EXPED_FILE $EXPED_DIR/$LAST_EXPED_FILE
else
  logit "-- DRY RUN MODE : $WORK_DIR/$LAST_EXPED_FILE file is left on directory : $WORK_DIR. No action has been processed."
fi

logit "Original file of the day had $(wc -l $WORK_DIR/$LAST_EXPED_FILE.orig | cut -d ' ' -f1) DCS to send "
logit "Backlog has $(wc -l $PATH_TO_WORK_BM_BACKLOG_FILE | cut -d ' ' -f1) DCS left to send "