#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/IntersolveWBX

function setParamsIntersolveWBX {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP SFTP sftp.intersolve.nl wonderbox '' Export '*.psv' delete
		setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} intersolve/in '*.psv'
		setLocalParams  ${jobs_scripts_tmp_path}/IntersolveWBX/ACT exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsIntersolveWBX $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
