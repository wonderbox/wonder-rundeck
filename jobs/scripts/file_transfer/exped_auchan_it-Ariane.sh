#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/auchan_it

function setParamsExpedAuchanIT {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /auchan_it/LIV '*.att' delete
		setDestFTP FTP fileport01.auchan.it wonderbox 1409Alighieri1321 download/LIV/ '*'
		setLocalParams ${jobs_scripts_tmp_path}/auchan_it/EXP exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP FTP WPPDFTP1 wonderbox Yuzi6Ahw /auchan_it/LIV '*.att' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/auchan_it/liv '*'
		setLocalParams ${jobs_scripts_tmp_path}/auchan_it/EXP tangi.vass@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsExpedAuchanIT $1
checkDryRunMode
checkReadiness
getFiles
for i in *.att ; do [ -f "$i" ] && touch ${i}.OK ; done
putFiles
notifyExped 'AUCHAN_IT EXPED'
moveFilesToBackup
cleanup

exit 0
