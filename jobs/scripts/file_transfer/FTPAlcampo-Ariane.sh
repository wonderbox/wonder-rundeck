#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/alcampo

function setParamsAlcampo {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP SFTP ftp.wonderbox.vpn alcampo_es s2h3L7Bk /upload/input '*.txt' delete
		setDestFTP FTP ftp.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /alcampo_es/input '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/tmp/alcampo/ACT exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP SFTP 192.168.111.11 wonderbox '' /home/ftp/wonderbox/alcampo_es/input '*.txt' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/alcampo_es/input '*.txt'
		setLocalParams ${jobs_scripts_tmp_path}/alcampo/ACT tangi.vass@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsAlcampo $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
