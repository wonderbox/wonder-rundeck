#!/bin/bash
# Script de transfert de fichiers entre EXALOG et wonderbox (projet EIFFEL)
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

BKUP_DIR_TAR=${backup_base_dir}/exalog/bank_statements/

SFTP_IDENTITY="/var/lib/rundeck/.ssh/old-keys/sched01/rundeck_id_rsa"

function setParamsEXA {
        setSrcFTP SFTP  ftpbank7.exalog.net wonderbox "" /success_recept '*releve*' delete "-P 7022"
        setDestFTP LOCAL wbx-prd-sched01.wonderbox.vpn rundeck "" /opt/cifs/exalog/bank_statements '*.*'
        setLocalParams ${jobs_scripts_tmp_path}/exalog/bank_statement it@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
}

main PROD
setParamsEXA
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
