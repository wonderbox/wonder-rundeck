#!/bin/bash

# Script de transfert de fichiers entre UBI et wonderbox (projet RFID)
# Sens : WBX->UBI
# Fichiers : Stock_PDV

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

BKUP_DIR_TAR=${backup_base_dir}/ubi/Stocks_PDV


function setParamsUBI {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/out/Stocks_PDV '*.zip' delete
		setDestFTP SFTP ubicloud.ubisolutions.net Wonderbox ssh_w0nderbo% /in/Stocks_PDV '*.zip'
		setLocalParams ${jobs_scripts_tmp_path}/ubi/Stocks_PDV exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	else
    setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/out/Stocks_PDV '*.zip' delete
    setDestFTP SFTP ubicloud-preprod.ubisolutions.net Wonderbox ssh_w0nderbo% /in/Stocks_PDV '*.zip'
    setLocalParams ${jobs_scripts_tmp_path}/ubi/Stocks_PDV exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	fi
}



main $1
setParamsUBI $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
ssh_w0nderbo%