#!/bin/bash

# Script de transfert de fichiers entre DIMO-SPA et wonderbox
# Sens : WBX->DIMO-SPA
# Fichiers : BaseMiroir

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/dimo-spa


function setParamsDIMO {
     
                setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /dimo-spa/output '*.psv' delete
                setDestFTP FTP ftp.dimospa.it wonderbox ptr993vbc /PRF '*.psv'
                setLocalParams ${jobs_scripts_tmp_path}/dimo-spa/Mirror exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
}


main $1
setParamsDIMO $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
