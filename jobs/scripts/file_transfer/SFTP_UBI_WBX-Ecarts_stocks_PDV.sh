#!/bin/bash

#Script de tranfert des fichiers d'expédition des assembleurs entre UBI et Wonderbox
# Sens UBI -> Wonderbox
# Fichiers ExpedLog

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

BKUP_DIR_TAR=${backup_base_dir}/ubi/expedlog

function setParamsUBI {
	if [ "$1" = "PROD" ]
	then
    setSrcFTP SFTP ubicloud.ubisolutions.net Wonderbox ssh_w0nderbo% /out/Ecarts_stocks_PDV '*' delete
    setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/in/Ecarts_stocks_PDV '*'
    setLocalParams ${jobs_scripts_tmp_path}/ubi/Ecarts_stocks_PDV exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	else
    setSrcFTP SFTP ubicloud-preprod.ubisolutions.net Wonderbox ssh_w0nderbo% /out/Ecarts_stocks_PDV '*' delete
    setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/in/Ecarts_stocks_PDV '*'
    setLocalParams ${jobs_scripts_tmp_path}/ubi/Ecarts_stocks_PDV exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	fi
}



main $1
setParamsUBI $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
