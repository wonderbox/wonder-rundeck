#!/bin/bash

# Script de transfert de fichiers entre UBI et wonderbox (projet RFID)
# Sens : WBX->UBI
# Fichiers : Tiers

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh

BKUP_DIR_TAR=${backup_base_dir}/ubi/Tiers


function setParamsUBI {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/out/Tiers '*.psv' delete
		setDestFTP SFTP ubicloud.ubisolutions.net Wonderbox ssh_w0nderbo% /in/Tiers '*.psv'
		setLocalParams ${jobs_scripts_tmp_path}/ubi/Tiers exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	else
    setSrcFTP FTP wbx-prd-ftp01.wonderbox.vpn wonderbox Yuzi6Ahw /ubi/out/Tiers '*.psv' delete
    setDestFTP SFTP ubicloud-preprod.ubisolutions.net Wonderbox ssh_w0nderbo% /in/Tiers '*.psv'
    setLocalParams ${jobs_scripts_tmp_path}/ubi/Tiers exploitation@wonderbox.fr $BKUP_DIR_TAR/`date +%Y/%m/%d`
	fi
}



main $1
setParamsUBI $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
