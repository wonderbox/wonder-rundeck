#!/bin/bash
#export PATH=/home/adminit/lftp/bin:$PATH

#Script de tranfert des fichiers logisticien entre TWS et Wonderbox
# ACHATS
script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/TWS_parameters.sh

BKUP_DIR_TAR=${TWS_BKUP_DIR}/ACHATS

function setParamsTWS {
	setSrcFTP ${WBX_PROTOCOL} ${WBX_GW} ${WBX_LOGIN} ${WBX_PWD} ${WBX_ROOT}/out/receptionlog 'ACHATS_*.psv' delete 21
	setDestFTP ${TWS_PROTOCOL} ${TWS_GW} ${TWS_LOGIN} ${TWS_PWD} /out/receptionlog 'ACHATS_*.psv' 21
	setLocalParams ${TWS_LCK_DIR}/ACHATS ${TWS_NOTIFY} $BKUP_DIR_TAR/`date +%Y/%m/%d`
}



main PROD
setParamsTWS 
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0
