#!/bin/bash

script=$(readlink -f "$0")
script_path=$(dirname "$script")
root_path=$(cd ${script_path}/.. && pwd)

source ${script_path}/../common/Ariane-Transfer-Functions.sh
source ${script_path}/../common/GLOBAL_parameters.sh

BKUP_DIR_TAR=${backup_base_dir}/carrefourBE

function setParamsCarrefourBE {
	if [ "$1" = "PROD" ]
	then
		setSrcFTP SFTP sftp.carrefourbelgium.be wonderbox '#w0nD3rB0Xx$2F7p' /prd/FromCB/ '*.psv' delete
		setDestFTP FTP wbx-prd-ftp01.wonderbox.vpn ${FTP_WBX_LOGIN} ${FTP_WBX_PWD} /carrefour_be/in '*.psv'
		setLocalParams ${jobs_scripts_tmp_path}/carrefour_be/ACT exploitation@wonderbox.fr ${BKUP_DIR_TAR}
	else
		setSrcFTP SFTP 192.168.111.11 wonderbox '' /home/ftp/wonderbox/carrefour_be/ '*.psv' keep
		setDestFTP FTP 192.168.111.11 wonderbox wOnderBo\\/ /dev/carrefour_be/in '*.psv'
		setLocalParams ${jobs_scripts_tmp_path}/carrefour_be/ACT tangi.vass@wonderbox.fr ${BKUP_DIR_TAR}
	fi
}



main $1
setParamsCarrefourBE $1
checkDryRunMode
checkReadiness
getFiles
putFiles
moveFilesToBackup
cleanup

exit 0 
