#!/bin/bash
# todo rename the parent directory to something like clean
#
# script qui efface tous les fichiers temporaires créés par l'api wondersales sauf les 10 plus récents
#

TMP_FILE="/tmp/windu-purge-tmp-file.txt"

ls -tc /tmp | grep "Job-Content-shortcut" | grep ".csv" > $TMP_FILE

nbL=`cat $TMP_FILE|wc -l`
if (( $nbL < 15 ));
then
  exit 0
fi
for i in `tail -n $((nbL-10)) $TMP_FILE`; do
  echo "rm -f /tmp/$i";
  rm -f /tmp/$i;
done

rm -f $TMP_FILE
