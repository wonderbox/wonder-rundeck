#!/bin/bash
export common_ariane_script=${BASH_SOURCE[0]}
export common_ariane_script_path=$(dirname "$common_ariane_script")
export scripts_path=$(cd ${common_ariane_script_path}/.. && pwd)
export jobs_path=$(cd ${scripts_path}/.. && pwd)

export jobs_scripts_tmp_path="${jobs_path}/tmp/scripts"
export backup_base_dir="${jobs_path}/backup" 
export logs_path="${jobs_path}/logs"
export IFS=$'\n'

source ${common_ariane_script_path}/check-connections.sh

# source global configuration file
if [ -f ${common_ariane_script_path}/../conf/setenv.sh ]; then
  source ${common_ariane_script_path}/../conf/setenv.sh
fi

if [ ! -d ${logs_path} ]; then
  mkdir -p ${logs_path}
fi

if [ -z  ${SFTP_IDENTITY} ]; then
  SFTP_IDENTITY="$HOME/.ssh/id_rsa"
fi

function main {

	cd `dirname $0`

	if [ "$1" = "" ]
	then
		echo "Usage:"
		echo "  `basename $0` [PROD|DEV]"
		exit 0
	else export ENVNAME=$1
	fi
}

function setSrcFTP {
	export SRC_TYPE="$1"
	export SRC_FTP_HOST="$2"
	export SRC_USERID="$3"
	export SRC_PASSWORD="$4"
    export SRC_FTP_BASE_DIR="$5"
    export SRC_FTP_FILE_PATTERN="$6"
    export SRC_FILES_ACTION="$7"
    export SRC_PORT_OPTION="$8"
	export FILES=
}

function setDestFTP {

	export DEST_TYPE="$1"
	export DEST_FTP_HOST="$2"
	export DEST_USERID="$3"
	export DEST_PASSWORD="$4"
    export DEST_FTP_BASE_DIR="$5"
    export DEST_FTP_FILE_PATTERN="$6"
	export DEST_PORT_OPTION="$7"    
}

function setLocalParams {

	export TMP_DIR="$1"
	export TARGETEMAILSTD="$2"
	export BKUP_DIR="$3"
	export SSL_CERT_FILE="$4"
	export SSL_CERTKEY_FILE="$5"
}

function lockJob {

	echo "========================================================================"
	echo "`date '+%Y/%m/%d %H:%M:%S'` : Démarrage de `basename $0` $ENVNAME"

	# Création du répertoire de travail
	if [ -d $TMP_DIR ] || ! mkdir -p $TMP_DIR || ! touch $TMP_DIR/.lock
	then
		ERROR="Impossible de créer le lock d'execution"
		echo $ERROR
		echo $TMP_DIR
        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Le processus `basename $0` de $1 ne peut s'exécuter sur le serveur $HOSTNAME car le traitmeent précédent ne s'est pas terminé correctmeent.
Verifier le repertoire $TMP_DIR qui peut contenir des fichiers non backupes.

Error:
'$ERROR'

Cordialement,
EOF
        exit 1

	fi
}

function unlockJob {

	echo "`date '+%Y/%m/%d %H:%M:%S'` : Arrêt de `basename $0` $ENVNAME"

	rm $TMP_DIR/.lock 2>/dev/null

	echo "========================================================================"
	echo
}

function checkReadiness {
	lockJob
}

function getFiles {

	echo Recuperer les fichiers du serveur remote de `basename $0 .sh` `date`

	cd $TMP_DIR
	if [ $? -ne 0 ]
    then
		export ERROR="Le processus $BASEDIR/`basename $0` ne parvient pas a aller dans le repertoire $TMP_DIR"

        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Error:
'$ERROR'

Cordialement,
EOF
	cleanup ; exit 1
	fi

	if [ "$SRC_TYPE" = "SFTP" ]
	then
		if [ "$SRC_PASSWORD" != "" ]
		then
			# exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
			a="lftp -u '${SRC_USERID},${SRC_PASSWORD}' -e 'cd $SRC_FTP_BASE_DIR && pwd && (mget $SRC_FTP_FILE_PATTERN || exit 0); quit' sftp://$SRC_FTP_HOST"
			eval $a
		else # le - devant le get sert à éviter de lever une erreur (donc un ocde retour > 0) quand on ne trouve pas de fichier
			cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - ${SRC_PORT_OPTION} ${SRC_USERID}@${SRC_FTP_HOST} # > /dev/null 2>&1
cd $SRC_FTP_BASE_DIR
pwd
-get $SRC_FTP_FILE_PATTERN
quit
EOF
		fi	
	elif [ "$SRC_TYPE" = "FTP" ]
	then 
		if [ "$SRC_FTP_HOST" = "f50ftp.carrefour.es" ]
		then
			# verrue pour la double authent carrefour
			( echo "user anonymous wonderbox" ; cat <<EOF ) | ftp -v -i -n $SRC_FTP_HOST # > /dev/null 2>&1
user $SRC_USERID $SRC_PASSWORD || exit 13
cd $SRC_FTP_BASE_DIR || exit 22
pwd
mget $SRC_FTP_FILE_PATTERN || exit 50
quit
EOF
		else
			cat <<EOF | lftp $SRC_FTP_HOST # > /dev/null 2>&1
set net:timeout 30
set net:max-retries 2
set net:reconnect-interval-base 1
set net:reconnect-interval-multiplier 1
set cmd:fail-exit true
user $SRC_USERID $SRC_PASSWORD || exit 13
cd $SRC_FTP_BASE_DIR || exit 22
pwd
mget $SRC_FTP_FILE_PATTERN || exit 50
quit
EOF
		fi
	elif [ "$SRC_TYPE" = "FTPS" ]
	then
		if [ "$SRC_PASSWORD" != "" ]
		then
			# exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
			lftp -u ${SRC_USERID},${SRC_PASSWORD} -p ${SRC_PORT_OPTION} ftp://${SRC_FTP_HOST} <<EOF
set ftp:ssl-allow true
set ftp:ssl-force ${SOURCE_SSL_FORCE}
set ftp:ssl-protect-data true
set ftp:ssl-protect-list true
set ssl:cert-file ${SSL_CERT_FILE}
set ssl:key-file ${SSL_CERTKEY_FILE}
set ssl:verify-certificate false
set ftps:initial-prot ""
set ftp:ssl-auth TLS
set ftp:ssl-use-ccc no
cd $SRC_FTP_BASE_DIR || exit 22 
pwd 
mget $SRC_FTP_FILE_PATTERN || exit 50
quit
EOF
		fi
	fi
	transferExitCode=$?
	if [ $transferExitCode -eq 50 ]
    then
    	echo "Pas de fichiers à télécharger"
	elif [ $transferExitCode -ne 0 ]
    then
		export ERROR="Le processus $BASEDIR/`basename $0` ne parvient pas a se connecter sur $SRC_FTP_HOST. Exit code=$transferExitCode"

        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Error:
'$ERROR'

Cordialement,
EOF
	cleanup ; exit 1
	fi
}

function unzipAndBackupFiles {

	mkdir -p $BKUP_DIR
	if [ $? -ne 0 ]
    then
		export ERROR="Le processus $BASEDIR/`basename $0` ne parvient pas a creer le répertoire $BKUP_DIR"

        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Error:
'$ERROR'

Cordialement,
EOF
	cleanup ; exit 1
	fi

	# Dezipper les fichiers
	export FILES=
	for f in `ls`
	do
		echo Dezipper et backuper le fichier $f
		tar xzf $f
		export FILES="$FILES $f"
		mv $f $BKUP_DIR
	done
}

function putFiles {

	# Tous les fichiers dans $TMP_DIR sont bien recuperes donc...
	cd $TMP_DIR

	if [ "`ls $DEST_FTP_FILE_PATTERN 2>/dev/null | wc -l`" -eq 0 ]
	then
		echo Pas de fichiers à transférer
		cleanup
		exit 0
	fi

	echo Transferer les fichiers "`ls $DEST_FTP_FILE_PATTERN`" vers la cible
  if [ "$DEST_TYPE" = "LOCAL" ]
  then
    echo "transferring file locally to ${DEST_FTP_BASE_DIR} from tmp dir [${TMP_DIR}]"
    cp -v -r ${DEST_FTP_FILE_PATTERN} ${DEST_FTP_BASE_DIR}
    
	elif [ "$DEST_TYPE" = "SFTP" ]
	then
		if [ "$DEST_PASSWORD" != "" ]
		then
			a="lftp -u '${DEST_USERID},${DEST_PASSWORD}' -e 'cd $DEST_FTP_BASE_DIR ; pwd ; mput $DEST_FTP_FILE_PATTERN ; quit' sftp://$DEST_FTP_HOST"
			eval $a
		else
			cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - ${DEST_PORT_OPTION} ${DEST_USERID}@${DEST_FTP_HOST} # > /dev/null 2>&1
cd $DEST_FTP_BASE_DIR
pwd
put $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	elif [ "$DEST_TYPE" = "FTP" ]
	then 
		if [ "$DEST_FTP_HOST" = "f50ftp.carrefour.es" ]
		then
			# verrue pour la double authent carrefour
			( echo "user anonymous wonderbox" ; cat <<EOF ) | ftp -v -i -n $DEST_FTP_HOST # >/dev/null 2>&1
user $DEST_USERID $DEST_PASSWORD
cd $DEST_FTP_BASE_DIR
pwd
mput $DEST_FTP_FILE_PATTERN
quit
EOF
		else
			cat <<EOF | lftp $DEST_FTP_HOST # >/dev/null 2>&1
set net:timeout 30
set net:max-retries 2
set net:reconnect-interval-base 1
set net:reconnect-interval-multiplier 1
set cmd:fail-exit true
user $DEST_USERID $DEST_PASSWORD
cd $DEST_FTP_BASE_DIR
pwd
mput $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	elif [ "$DEST_TYPE" = "FTPS" ]
	then
		if [ "$DEST_PASSWORD" != "" ]
		then
			# exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
			lftp -u ${DEST_USERID},${DEST_PASSWORD} -p ${DEST_PORT_OPTION} ftp://${DEST_FTP_HOST} <<EOF
set ftp:ssl-allow true
set ftp:ssl-force ${DESTINATION_SSL_FORCE}
set ftp:ssl-protect-data true
set ftp:ssl-protect-list true
set ssl:cert-file ${SSL_CERT_FILE}
set ssl:key-file ${SSL_CERTKEY_FILE}
set ssl:verify-certificate false
set ftps:initial-prot ""
set ftp:ssl-auth TLS
set ftp:ssl-use-ccc no
cd $DEST_FTP_BASE_DIR
pwd
mput $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	fi

	if [ $? -ne 0 ]
    then
		export ERROR="Le processus $BASEDIR/`basename $0` ne parvient pas a se connecter sur $DEST_FTP_HOST"

        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Error:
'$ERROR'

Cordialement,
EOF
	cleanup ; exit 1
	fi

	# Marquage à la source des fichiers transférés

	if [ "$FILES" = "" ] # FILES contient la liste des fichiers tar.gz s'il y en a
	then
		export FILES=`ls`
	fi	

	for f in $FILES
	do
		if [[ "$SRC_FILES_ACTION" = "delete" ]]
		then
			echo Supprimer $f à la source

			if [ "$SRC_TYPE" = "SFTP" ]
			then
				if [ "$SRC_PASSWORD" != "" ]
				then
					a="lftp -u '${SRC_USERID},${SRC_PASSWORD}' -e 'cd $SRC_FTP_BASE_DIR ; rm $f ; quit' sftp://$SRC_FTP_HOST"
					eval $a
				else
					cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - ${SRC_PORT_OPTION} ${SRC_USERID}@${SRC_FTP_HOST} # > /dev/null 2>&1
cd $SRC_FTP_BASE_DIR
pwd
rm '$f'
quit
EOF
				fi
			elif [ "$SRC_TYPE" = "FTP" ]
			then 
				if [ "$SRC_FTP_HOST" = "f50ftp.carrefour.es" ]
				then
					# verrue pour la double authent carrefour
					( 	echo "user anonymous wonderbox" ; cat <<EOF ) | ftp -v -i -n $SRC_FTP_HOST # >/dev/null 2>&1
user $SRC_USERID $SRC_PASSWORD
cd $SRC_FTP_BASE_DIR
delete $f
quit
EOF
				else
					cat <<EOF | lftp $SRC_FTP_HOST # >/dev/null 2>&1
set net:timeout 30
set net:max-retries 2
set net:reconnect-interval-base 1
set net:reconnect-interval-multiplier 1
set cmd:fail-exit true
user $SRC_USERID $SRC_PASSWORD
cd $SRC_FTP_BASE_DIR
pwd
rm $f
quit
EOF
				fi
			elif [ "$SRC_TYPE" = "FTPS" ]
			then
				if [ "$SRC_PASSWORD" != "" ]
				then
					# exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
					lftp -u ${SRC_USERID},${SRC_PASSWORD} -p ${SRC_PORT_OPTION} ftp://${SRC_FTP_HOST} <<EOF
set ftp:ssl-allow true
set ftp:ssl-force ${SOURCE_SSL_FORCE}
set ftp:ssl-protect-data true
set ftp:ssl-protect-list true
set ssl:cert-file ${SSL_CERT_FILE}
set ssl:key-file ${SSL_CERTKEY_FILE}
set ssl:verify-certificate false
set ftps:initial-prot ""
set ftp:ssl-auth TLS
set ftp:ssl-use-ccc no
cd $SRC_FTP_BASE_DIR
pwd
rm $f
quit
EOF
				fi
			fi
#		elif [ "$SRC_FILES_ACTION" = "archive" ]
#		then 
#			echo Deplacer $f à la source vers le répertoire archive

			# TODO

		else
			echo Laisser $f à la source
		fi
	done
}

function notifyExped {

	SUBJECT="$1 $ENVNAME $(date +%Y%m%d)"
	ATTACHEMENTS=`ls | awk -F'.' '{ print $NF }' | egrep '^(att|txt|csv)$' | sort -u | sed 's/^/-a *./' | tr '\n' ' '`

	if [ "$ATTACHEMENTS" != "" ]
	then 
		echo "* Sending mail"

		cat <<EOF | mutt -s "$SUBJECT" $ATTACHEMENTS -- $TARGETEMAILSTD
Bonjour,

Il y a $(cat *.{att,txt,csv} 2>/dev/null | wc -l | cut -f 1 -d' ') lignes au total dans les fichiers.

Cordialement,
EOF

	fi
}

function moveFilesToBackup {

	echo Backuper les fichiers * sous $BKUP_DIR
	
	mkdir -p $BKUP_DIR && mv * $BKUP_DIR

	if [ $? -ne 0 ]
    then
		export ERROR="Le processus $BASEDIR/`basename $0` ne parvient pas a creer le répertoire $BKUP_DIR"

        cat <<EOF | mutt -s "Erreur avec le processus `basename $0` sur $HOSTNAME." $TARGETEMAILSTD
Bonjour,

Error:
'$ERROR'

Cordialement,
EOF
	unlockJob ; exit 1 # on ne fait pas le cleanup car le transfert vers DESt a réussi mais pas le backup -> le faire manuellement
	fi	
}

function cleanup {

	unlockJob
	[ "$TMP_DIR" != "" ] && rm -f $TMP_DIR/*
	rmdir $TMP_DIR
}

