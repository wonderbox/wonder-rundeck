#!/bin/bash

checkSourceServer() {
  echo "Test des connexions vers les serveurs pour Recuperer les fichiers du serveur remote de $(basename $0 .sh) $(date)"

  if [ "$SRC_TYPE" = "SFTP" ]; then
    if [ "$SRC_PASSWORD" != "" ]; then
      # exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
      a="lftp -u '${SRC_USERID},${SRC_PASSWORD}' -e 'cd $SRC_FTP_BASE_DIR && ls -l $SRC_FTP_FILE_PATTERN; quit' sftp://$SRC_FTP_HOST"
      eval $a
    else # le - devant le get sert à éviter de lever une erreur (donc un ocde retour > 0) quand on ne trouve pas de fichier
      cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - ${SRC_PORT_OPTION} ${SRC_USERID}@${SRC_FTP_HOST} # > /dev/null 2>&1
cd $SRC_FTP_BASE_DIR
pwd
-ls -l $SRC_FTP_FILE_PATTERN
quit
EOF
    fi
  elif [ "$SRC_TYPE" = "FTP" ]; then
    if [ "$SRC_FTP_HOST" = "f50ftp.carrefour.es" ]; then
      # verrue pour la double authent carrefour
      (
        echo "user anonymous wonderbox"
        cat <<EOF
user $SRC_USERID $SRC_PASSWORD || exit 13
cd $SRC_FTP_BASE_DIR || exit 22
pwd
ls -l $SRC_FTP_FILE_PATTERN
quit
EOF
      ) | ftp -v -i -n $SRC_FTP_HOST # > /dev/null 2>&1

    else

      cat <<EOF | lftp $SRC_FTP_HOST # > /dev/null 2>&1
set net:timeout 30
set net:max-retries 2
set net:reconnect-interval-base 1
set net:reconnect-interval-multiplier 1
set cmd:fail-exit true
user $SRC_USERID $SRC_PASSWORD || exit 13
cd $SRC_FTP_BASE_DIR || exit 22
pwd
ls -l $SRC_FTP_FILE_PATTERN
quit
EOF
    fi
  elif [ "$SRC_TYPE" = "FTPS" ]; then
    if [ "$SRC_PASSWORD" != "" ]; then
      # exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
      lftp -u ${SRC_USERID},${SRC_PASSWORD} -p ${SRC_PORT_OPTION} ftp://${SRC_FTP_HOST} <<EOF
set ftp:ssl-allow true
set ftp:ssl-force ${SOURCE_SSL_FORCE}
set ftp:ssl-protect-data true
set ftp:ssl-protect-list true
set ssl:cert-file ${SSL_CERT_FILE}
set ssl:key-file ${SSL_CERTKEY_FILE}
set ssl:verify-certificate false
set ftps:initial-prot ""
set ftp:ssl-auth TLS
set ftp:ssl-use-ccc no
cd $SRC_FTP_BASE_DIR || exit 22 
pwd 
ls -l $SRC_FTP_FILE_PATTERN 
quit
EOF
    fi
  fi
  transferExitCode=$?
  if [ $transferExitCode -eq 50 ]; then
    echo "Pas de fichiers à télécharger"
  elif [ $transferExitCode -ne 0 ]; then
    echo "Le processus $BASEDIR/$(basename $0) ne parvient pas a se connecter sur $SRC_FTP_HOST. Exit code=$transferExitCode"
    exit 1
  fi
}
checkDestinationServer() {
  
	echo Test de Transfer de fichiers  vers  ${DEST_FTP_BASE_DIR} en mode  ${DEST_TYPE} vers la cible ${DEST_FTP_HOST}
  if [ "$DEST_TYPE" = "LOCAL" ]
  then
    echo "transferring file locally to ${DEST_FTP_BASE_DIR} from tmp dir [${TMP_DIR}]"
    ls $DEST_FTP_BASE_DIR
    
	elif [ "$DEST_TYPE" = "SFTP" ]
	then
		if [ "$DEST_PASSWORD" != "" ]
		then
			a="lftp -u '${DEST_USERID},${DEST_PASSWORD}' -e 'cd $DEST_FTP_BASE_DIR ; pwd ; ls -l $DEST_FTP_FILE_PATTERN; quit' sftp://$DEST_FTP_HOST"
			eval $a
		else
			cat <<EOF | sftp -i ${SFTP_IDENTITY} -b - ${DEST_PORT_OPTION} ${DEST_USERID}@${DEST_FTP_HOST} # > /dev/null 2>&1
cd $DEST_FTP_BASE_DIR
pwd
ls -l $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	elif [ "$DEST_TYPE" = "FTP" ]
	then 
		if [ "$DEST_FTP_HOST" = "f50ftp.carrefour.es" ]
		then
			# verrue pour la double authent carrefour
			( echo "user anonymous wonderbox" ; cat <<EOF ) | ftp -v -i -n $DEST_FTP_HOST # >/dev/null 2>&1
user $DEST_USERID $DEST_PASSWORD
cd $DEST_FTP_BASE_DIR
pwd
ls -l $DEST_FTP_FILE_PATTERN
quit
EOF
		else
			cat <<EOF | lftp $DEST_FTP_HOST # >/dev/null 2>&1
set net:timeout 30
set net:max-retries 2
set net:reconnect-interval-base 1
set net:reconnect-interval-multiplier 1
set cmd:fail-exit true
user $DEST_USERID $DEST_PASSWORD
cd $DEST_FTP_BASE_DIR
pwd
ls -l $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	elif [ "$DEST_TYPE" = "FTPS" ]
	then
		if [ "$DEST_PASSWORD" != "" ]
		then
			# exit 0 quand aucun fichier à télécharger, cd sortira en 1 si pb de login/connexion/répertoire manquant etc
			lftp -u ${DEST_USERID},${DEST_PASSWORD} -p ${DEST_PORT_OPTION} ftp://${DEST_FTP_HOST} <<EOF
set ftp:ssl-allow true
set ftp:ssl-force ${DESTINATION_SSL_FORCE}
set ftp:ssl-protect-data true
set ftp:ssl-protect-list true
set ssl:cert-file ${SSL_CERT_FILE}
set ssl:key-file ${SSL_CERTKEY_FILE}
set ssl:verify-certificate false
set ftps:initial-prot ""
set ftp:ssl-auth TLS
set ftp:ssl-use-ccc no
cd $DEST_FTP_BASE_DIR
pwd
ls -l $DEST_FTP_FILE_PATTERN
quit
EOF
		fi
	fi

	if [ $? -ne 0 ]
    then
		echo "Le processus $BASEDIR/`basename $0` ne parvient pas a se connecter sur $DEST_FTP_HOST"      
	  exit 1
	fi
}

function checkConnexionsOnly() {
 checkSourceServer
 checkDestinationServer
}

function checkDryRunMode(){
  if [ "${DRY_RUN}" == "true" ]; then
    checkConnexionsOnly
    exit 0 
  fi
}